/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author informatics
 */
public class User {

    int userId;
    String loginName;
    String password;
    String name;
    String surname;
    int typId;

    public User(int userId, String loginName,  String name, String surname, String password, int typId) {
        this.userId = userId;
        this.loginName = loginName;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.typId = typId;
    }

    public User() {
        
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getTypId() {
        return typId;
    }

    public void setTypId(int typId) {
        this.typId = typId;
    }
    
    public String toString(){ 
        return "user{"+ "userId=" + userId +", loginName=" + loginName + ", password=" + password + ", name=" + name + ", surname" + surname + ", password" + password + ", typId=" + typId + "}";
    }
}
